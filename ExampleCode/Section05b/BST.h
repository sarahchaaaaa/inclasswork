/**********************************************
* File: BST.h
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
* Binary Search Tree Class
* Modified Class from https://users.cs.fiu.edu/~weiss/dsaa_c++4/code
**********************************************/
#ifndef BST_H
#define BST_H

#include "dsexceptions.h"
#include <algorithm>
using namespace std;       

template <typename T>
class BST
{
  public:
    /********************************************
    * Function Name  : BST
    * Pre-conditions :  
    * Post-conditions: none
    *  
    ********************************************/
    BST( ) : root{ nullptr }
    {
    }

    /********************************************
    * Function Name  : BST
    * Pre-conditions :  const BST & rhs 
    * Post-conditions: none
    *  
	* Copy constructor
    ********************************************/
    BST( const BST & rhs ) : root{ nullptr }
    {
        root = clone( rhs.root );
    }

    /********************************************
    * Function Name  : BST
    * Pre-conditions :  BST && rhs 
    * Post-conditions: none
    *  Move constructor
    ********************************************/
    BST( BST && rhs ) : root{ rhs.root }
    {
        rhs.root = nullptr;
    }
    
    /********************************************
    * Function Name  : ~BST
    * Pre-conditions :  
    * Post-conditions: none
    *  Destructor for the tree
    ********************************************/
    ~BST( )
    {
        makeEmpty( );
    }

    /********************************************
    * Function Name  : operator=
    * Pre-conditions :  const BST & rhs 
    * Post-conditions: BST &
    * Copy assignment 
    ********************************************/
    BST & operator=( const BST & rhs )
    {
        BST copy = rhs;
        std::swap( *this, copy );
        return *this;
    }
        

    /********************************************
    * Function Name  : operator=
    * Pre-conditions :  BST && rhs 
    * Post-conditions: BST &
    *  Move assignment
    ********************************************/
    BST & operator=( BST && rhs )
    {
        std::swap( root, rhs.root );       
        return *this;
    }
    
    
    /********************************************
    * Function Name  : findMin
    * Pre-conditions :  
    * Post-conditions: const T &
    * Find the smallest item in the tree.
    * Throw UnderflowException if empty.  
    ********************************************/
    const T & findMin( ) const
    {
        if( isEmpty( ) )
            throw UnderflowException{ };
        return findMin( root )->element;
    }

    /**
     * Find the largest item in the tree.
     * Throw UnderflowException if empty.
     */
    /********************************************
    * Function Name  : findMax
    * Pre-conditions :  
    * Post-conditions: const T &
    *  Find the largest item in the tree.
    *  Throw UnderflowException if empty.
    ********************************************/
    const T & findMax( ) const
    {
        if( isEmpty( ) )
            throw UnderflowException{ };
        return findMax( root )->element;
    }

    /********************************************
    * Function Name  : contains
    * Pre-conditions :  const T & x 
    * Post-conditions: bool
    * Returns true if x is found in the tree.
    ********************************************/
    bool contains( const T & x ) const
    {
        return contains( x, root );
    }


    /********************************************
    * Function Name  : isEmpty
    * Pre-conditions :  
    * Post-conditions: bool
     * Test if the tree is logically empty.
     * Return true if empty, false otherwise.
    ********************************************/
    bool isEmpty( ) const
    {
        return root == nullptr;
    }

    /**
     * Print the tree contents in sorted order.
     */
    /********************************************
    * Function Name  : printTree
    * Pre-conditions :  ostream & out = cout 
    * Post-conditions: none
     * Print the tree contents in sorted order.
    ********************************************/
    void printTree( ostream & out = cout ) const
    {
        if( isEmpty( ) )
            out << "Empty tree" << endl;
        else
            printTree( root, out );
    }

    /********************************************
    * Function Name  : makeEmpty
    * Pre-conditions :  
    * Post-conditions: none
    * Make the tree logically empty.
    ********************************************/
    void makeEmpty( )
    {
        makeEmpty( root );
    }

    /********************************************
    * Function Name  : insert
    * Pre-conditions :  const T & x 
    * Post-conditions: none
    * 
	* Insert x into the tree; duplicates are ignored.
    ********************************************/
    void insert( const T & x )
    {
        insert( x, root );
    }
     
    /********************************************
    * Function Name  : insert
    * Pre-conditions :  T && x 
    * Post-conditions: none
    *  
	* Insert x into the tree; duplicates are ignored.
    ********************************************/
    void insert( T && x )
    {
        insert( std::move( x ), root );
    }
    
    /********************************************
    * Function Name  : remove
    * Pre-conditions :  const T & x 
    * Post-conditions: none
    *  
	* Remove x from the tree. Nothing is done if x is not found.
    ********************************************/
    void remove( const T & x )
    {
        remove( x, root );
    }


  private:
    struct BSTNode
    {
        T element;
        BSTNode *left;
        BSTNode *right;

        /********************************************
        * Function Name  : BSTNode
        * Pre-conditions :  const T & theElement, BSTNode *lt, BSTNode *rt 
        * Post-conditions: none
        * 
		* BSTNode Constructor
        ********************************************/
        BSTNode( const T & theElement, BSTNode *lt, BSTNode *rt )
          : element{ theElement }, left{ lt }, right{ rt } { }
        
        /********************************************
        * Function Name  : BSTNode
        * Pre-conditions :  T && theElement, BSTNode *lt, BSTNode *rt 
        * Post-conditions: none
        *  
		* BSTNode Constructor with T moved 
        ********************************************/
        BSTNode( T && theElement, BSTNode *lt, BSTNode *rt )
          : element{ std::move( theElement ) }, left{ lt }, right{ rt } { }
    };

    BSTNode *root;


    /**
     * Internal method to insert into a subtree.
     * x is the item to insert.
     * t is the node that roots the subtree.
     * Set the new root of the subtree.
     */
    void insert( const T & x, BSTNode * & t )
    {
        if( t == nullptr )
            t = new BSTNode{ x, nullptr, nullptr };
        else if( x < t->element )
            insert( x, t->left );
        else if( t->element < x )
            insert( x, t->right );
        else
            ;  // Duplicate; do nothing
    }
    
    /**
     * Internal method to insert into a subtree.
     * x is the item to insert.
     * t is the node that roots the subtree.
     * Set the new root of the subtree.
     */
    void insert( T && x, BSTNode * & t )
    {
        if( t == nullptr )
            t = new BSTNode{ std::move( x ), nullptr, nullptr };
        else if( x < t->element )
            insert( std::move( x ), t->left );
        else if( t->element < x )
            insert( std::move( x ), t->right );
        else
            ;  // Duplicate; do nothing
    }

    /**
     * Internal method to remove from a subtree.
     * x is the item to remove.
     * t is the node that roots the subtree.
     * Set the new root of the subtree.
     */
    void remove( const T & x, BSTNode * & t )
    {
        if( t == nullptr )
            return;   // Item not found; do nothing
        if( x < t->element )
            remove( x, t->left );
        else if( t->element < x )
            remove( x, t->right );
        else if( t->left != nullptr && t->right != nullptr ) // Two children
        {
            t->element = findMin( t->right )->element;
            remove( t->element, t->right );
        }
        else
        {
            BSTNode *oldNode = t;
            t = ( t->left != nullptr ) ? t->left : t->right;
            delete oldNode;
        }
    }

    /**
     * Internal method to find the smallest item in a subtree t.
     * Return node containing the smallest item.
     */
    BSTNode * findMin( BSTNode *t ) const
    {
        if( t == nullptr )
            return nullptr;
        if( t->left == nullptr )
            return t;
        return findMin( t->left );
    }

    /**
     * Internal method to find the largest item in a subtree t.
     * Return node containing the largest item.
     */
    BSTNode * findMax( BSTNode *t ) const
    {
        if( t != nullptr )
            while( t->right != nullptr )
                t = t->right;
        return t;
    }


    /**
     * Internal method to test if an item is in a subtree.
     * x is item to search for.
     * t is the node that roots the subtree.
     */
    bool contains( const T & x, BSTNode *t ) const
    {
        if( t == nullptr )
            return false;
        else if( x < t->element )
            return contains( x, t->left );
        else if( t->element < x )
            return contains( x, t->right );
        else
            return true;    // Match
    }

    /**
     * Internal method to make subtree empty.
     */
    void makeEmpty( BSTNode * & t )
    {
        if( t != nullptr )
        {
            makeEmpty( t->left );
            makeEmpty( t->right );
            delete t;
        }
        t = nullptr;
    }

    /**
     * Internal method to print a subtree rooted at t in sorted order.
     */
    void printTree( BSTNode *t, ostream & out ) const
    {
        if( t != nullptr )
        {
            printTree( t->left, out );
            out << t->element << endl;
            printTree( t->right, out );
        }
    }

    /**
     * Internal method to clone subtree.
     */
    BSTNode * clone( BSTNode *t ) const
    {
        if( t == nullptr )
            return nullptr;
        else
            return new BSTNode{ t->element, clone( t->left ), clone( t->right ) };
    }
};

#endif
